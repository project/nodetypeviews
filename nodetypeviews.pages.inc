<?php
/**
 * @file
 * Page callback file for the Node Type Views module.
 */

/**
 * Create the teaser list for the given node type
 *
 * @param String $type
 * @return String $output - the HTML markup of the view
 */
function nodetypeviews_type_teaser_view($type) {
  if (!nodetypeviews_check_type($type)) {
    return '';
  }

  $build = array();

  $query = db_select('node', 'n')->extend('PagerDefault');
  $nids = $query
    ->fields('n', array('nid', 'sticky', 'created'))
    ->condition('type', $type)
    ->condition('status', 1)
    ->orderBy('sticky', 'DESC')
    ->orderBy('created', 'DESC')
    ->limit(variable_get('default_nodes_main', 10))
    ->addTag('node_access')
    ->execute()
    ->fetchCol();

  if (!empty($nids)) {
    $nodes = node_load_multiple($nids);
    $build += node_view_multiple($nodes);
    $build['pager'] = array(
      '#theme' => 'pager',
      '#weight' => 5,
    );
  }
  else {
    drupal_set_message(t('No @type entries have been created.',
      array('@type' => $type)));
  }

  drupal_add_feed(check_plain($type) . '/feed', t('RSS - @type',
      array('@type' => $type)));

  return $build;
}

/**
 * Create an RSS feed for the given node type
 *
 * @param String $type
 * @return String - the XML feed
 */
function nodetypeviews_type_teaser_feed($type) {
  if (!nodetypeviews_check_type($type)) {
    return '';
  }

  $nids = db_select('node', 'n')
    ->fields('n', array('nid', 'created'))
    ->condition('type', $type)
    ->condition('status', 1)
    ->orderBy('created', 'DESC')
    ->range(0, variable_get('feed_default_items', 10))
    ->addTag('node_access')
    ->execute()
    ->fetchCol();

  $channel['title'] = variable_get('site_name', 'Drupal') . ' ' . check_plain($type);
  $channel['link'] = url($type, array('absolute' => TRUE));

  node_feed($nids, $channel);
}