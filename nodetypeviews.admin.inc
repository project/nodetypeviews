<?php
/**
 * @file
 * Admin settings for the Node Type Views module.
 */

/**
 * Admin settings form
 */
function nodetypeviews_admin_settings() {
  $form = array();
  $form['nodetypeviews_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#default_value' => variable_get('nodetypeviews_node_types', array()),
    '#options' => array_map('check_plain', node_type_get_names()),
    '#description' => t("Select the content types to generate views for. Do not enable Blog entry, unless it is a content type you created the blog module takes care of that.")
  );
  return system_settings_form($form);
}